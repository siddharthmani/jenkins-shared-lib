def call (setvariables) {
    env.sfUsrName   		= "sid_2@cloud.com" //username to be used for deployment
    env.sfSandbox 			= "https://test.salesforce.com" //URL for sandbox
	env.sfProd    			= "https://login.salesforce.com" //URL for production
	env.sfJWTKeyDev 		= "sfdx-demo-serverkey" //ID reference to secret key file stored in Jenkins credentials
	env.sfConKeyDev 		= "3MVG9G9pzCUSkzZvFN7ydOT_Nbx2PtPNMiotlEEgO5hlkmWxR1ixPNvnN9BRCbuR.mSRxQOU3830SZV6hovWF" //Consumer key value from Salesforce connected app
	env.sfDeployDir 		= "force-app" //folder to be deployed
	env.sfTestLevel 		= "NoTestRun" //Test level - valid values are RunLocalTests, RunSpecifiedTests, RunAllTestsInOrg, NoTestRun
	env.sfCheckoutBranch 	= "Develop" //Branch being checked out as part of git command
	env.sfBranchToMerge 	= "Release"	//Branch to merge into in case of Preprod deployment
}

def sfcodepull(String gitlab_branch, String gitlab_repo){
     env.gitlab_credentials = "sid-gitlab-personal-creds"
     git branch: "${gitlab_branch}", credentialsId: "${env.gitlab_credentials}",  url: "${gitlab_repo}"
}

def sfAuthenticateJWT(String consumerKey, String userName, String jwtfileref, String hostName){
    def toolbelt = tool 'toolbelt'
    withCredentials([file(credentialsId: "${jwtfileref}", variable: 'jwt_key_file')]) {
        rc = sh returnStatus: true, script: "${toolbelt} force:auth:jwt:grant --clientid ${consumerKey} --username ${userName} --jwtkeyfile ${jwt_key_file} --setdefaultdevhubusername --instanceurl ${hostName}"
    }
}

def sfDeploy(String jwtfileref, String folderToDeploy, String testLevel, String userName, String checkOnly){
    def toolbelt = tool 'toolbelt'
    if(checkOnly=='Yes'){
        sh "${toolbelt} force:source:deploy -p ./${folderToDeploy} -l ${testLevel} -u ${userName} -c"
    }else{
        sh "${toolbelt} force:source:deploy -p ./${folderToDeploy} -l ${testLevel} -u ${userName}"
    }
}

def sfLogout(String userName){
    def toolbelt = tool 'toolbelt'
	sh "${toolbelt} force:auth:logout --targetusername ${userName} --noprompt"
}

//def sfcodepull(String name = 'human') {
    // Any valid steps can be called from this code, just like in other
    // Scripted Pipeline
  //  echo "Hello, ${name}."
//}




//Code from Xcel Gitlab repo
/*
def call(setvariables) {
    env.sfUsrNameDev   		= "cext_jira_gitlab_integration@xcelenergy.com.dev" //username to be used for deployment
	env.sfUsrNameQA   		= "cext_jira_gitlab_integration@xcelenergy.com.qa" //username to be used for deployment
	env.sfUsrNamePreprod	= "cext_jira_gitlab_integration@xcelenergy.com.preprod" //username to be used for deployment
	env.sfUsrNameCXTTeam	= "siddharth.mani@xcelenergy.com.cxtteam" //username to be used for deployment
	env.sfUsrNameProd   	= "cext_jira_gitlab_integration@xcelenergy.com" //username to be used for deployment
    env.sfSandbox 			= "https://test.salesforce.com" //URL for sandbox
	env.sfProd    			= "https://login.salesforce.com" //URL for production
	env.sfJWTKeyDev 		= "sfdx-serverkeyfile-dev" //ID reference to secret key file stored in Jenkins credentials
	env.sfJWTKeyQA 			= "sfdx-serverkeyfile-qa" //ID reference to secret key file stored in Jenkins credentials
	env.sfJWTKeyPreprod 	= "sfdx-serverkeyfile-preprod"	//ID reference to secret key file stored in Jenkins credentials
	env.sfJWTKeyCXTTeam 	= "sfdx-serverkeyfile-cxteam"	//ID reference to secret key file stored in Jenkins credentials
	env.sfJWTKeyProd     	= "sfdx-serverkeyfile-prod"	//ID reference to secret key file stored in Jenkins credentials
	env.sfConKeyDev 		= "3MVG9RezSyZYLh2vwWhpheSlcFznoSXAZhGkHudNx74nJVnkX3ExQLor_8suL_e2.qMZ315ojL1T7Z6tDgg.o" //Consumer key value from Salesforce connected app
	env.sfConKeyQA 			= "3MVG9Nc1qcZ7BbZ0KjpbJ.z4IpystCJLP3VOmHZhnFqK83WdvkAGilgzU6oZr.xLPQdN7s6rtnk9gdWm3C9b0" //Consumer key value from Salesforce connected app
	env.sfConKeyPreprod 	= "3MVG9Nc1qcZ7BbZ1PuOtx_xhZpvpTOIR3YsdUxkKN1KBHyMuLbHm2BPB8q_Ti_4_U2U4fxaUtOHzDukBDdTNm" //Consumer key value from Salesforce connected app
	env.sfConKeyCXTTeam 	= "3MVG9QBLg8QGkFepThVGu_AFRYJF3mM0.89xc.Jl0SXcOizsUp.xddhvqGL9FcVVFKlSAArBmVF.k9YHU4vue" //Consumer key value from Salesforce connected app
	env.sfConKeyProd 	    = "3MVG9KsVczVNcM8w5fQxSPPsa7yEHRT_VNHmeMuA5rtz_qJSdQ611aBn8w8p5OhIrKnQI4mJG0PpXQiynhF1Z" //Consumer key value from Salesforce connected app
}

def sfCodePull(String gitlab_branch, String gitlab_repo){
    env.gitlab_credentials = "52c0d6dc-a82f-4c89-901f-b90a2b10ff4c"
    	//env.gitlab_credentials ="dd6de4cf-fb5c-4f03-ac11-de680e6029cb" //svc account doesn't work
    //env.gitlab_credentials = "sid-gitlab-saas-cred"
    git branch: "${gitlab_branch}", credentialsId: "${env.gitlab_credentials}",  url: "${gitlab_repo}"
}

def sfCodeMerge(String checkoutBranch, String branchToMerge, String gitlab_repo, String buildNumber){
    //env.gitlab_credentials = "sid-gitlab-saas-cred"
    env.gitlab_credentials = "52c0d6dc-a82f-4c89-901f-b90a2b10ff4c"
    git branch: "${checkoutBranch}", credentialsId: "${env.gitlab_credentials}",  url: "${gitlab_repo}"
    withCredentials([usernamePassword(credentialsId: "${env.gitlab_credentials}", usernameVariable: 'username', passwordVariable: 'password')]){
            rc  = sh "git tag -a tag-`date +%Y-%m-%d_%H/%M/%S` -m 'Build on \'`date +%Y-%m-%d:%H:%M:%S`\' for Release ${buildNumber}'"
            rc2 = sh "git merge origin/${branchToMerge} origin/${checkoutBranch}"
            rc3 = sh "git push https://${username}:${password}@gitlab.com/cxt-sfdc-group/cxt-salesforce-code.git ${checkoutBranch} --tags"
        }
}

def sfAuthenticateJWT(String consumerKey, String userName, String jwtfileref, String hostName){
    def toolbelt = tool 'toolbelt'
    withCredentials([file(credentialsId: "${jwtfileref}", variable: 'jwt_key_file')]) {
        rc = sh returnStatus: true, script: "${toolbelt} force:auth:jwt:grant --clientid ${consumerKey} --username ${userName} --jwtkeyfile ${jwt_key_file} --setdefaultdevhubusername --instanceurl ${hostName}"
    }
}

def sfDeploy(String jwtfileref, String folderToDeploy, String testLevel, String userName, String checkOnly){
    def toolbelt = tool 'toolbelt'
    if(checkOnly=='Yes'){
        sh "${toolbelt} force:source:deploy -p ./${folderToDeploy} -l ${testLevel} -u ${userName} -c"
    }else{
        sh "${toolbelt} force:source:deploy -p ./${folderToDeploy} -l ${testLevel} -u ${userName}"
    }
}

def sfLogout(String userName){
    def toolbelt = tool 'toolbelt'
	sh "${toolbelt} force:auth:logout --targetusername ${userName} --noprompt"
}

*/