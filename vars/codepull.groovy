def call(body){
    env.gitlab_credentials = 'dd6de4cf-fb5c-4f03-ac11-de680e6029cb'
	def config = [:]
        body.resolveStrategy = Closure.DELEGATE_FIRST
        body.delegate = config
        body()
		
		stage('Jenkins Workspace details') {

            echo "Workspace: ${env.WORKSPACE} with Jenkins home at ${env.JENKINS_HOME}"
            echo "Environment will be : ${env.NODE_ENV}"
            def lsoutput = sh returnStdout: true, script: 'ls'
            def pwdoutput = sh returnStdout: true, script: 'pwd'
            echo "Current directory location is: ${pwdoutput}"
            echo "Current workspace contents: ${lsoutput}"

           
          }
          
          stage('Gitlab Code Pull') {
          deleteDir()
            
                git branch: "${env.gitlab_branch}", credentialsId: "${env.gitlab_credentials}",  url: "${env.gitlab_repo}"
                env.VERSION = sh returnStdout: true, script: 'git describe --always'
                sh "whoami"
                if (env.VERSION) {
                  echo "Building version ${env.VERSION}"
              }
         }


		
		
  }